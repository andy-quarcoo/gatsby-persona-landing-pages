import React, {Component} from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import "../components/main.css"
import logo from "../components/logo.png"
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Navbar from "../components/navbar";
import Background from "../images/suppliers/supplier-three.jpg"
import "../components/bootstrap/css/bootstrap.min.css"
import firebase from "../components/firebase";


class ThirdPage extends Component{

    constructor(props){
        super(props);
        this.state = {
            name: '',
            organization:'',
            phone:'',
            email:'',
            date_posted: firebase.database.ServerValue.TIMESTAMP,
            personna:3
        };

        this.baseState = this.state;
    }

    resetForm = () => {
        this.setState(this.baseState)
    };

    handleClick = (e) => {
        e.preventDefault();
        this.resetForm();
        const db = firebase.database();
        //get a new post key
        const newPostKey = db.ref().child('posts').push().key;
        db.ref('posts/' + newPostKey).set(this.state,
            function (error) {
                if (error)
                {
                    alert("Sorry, problem sending your detail")
                }
                else
                {
                    alert("Data successfully sent")
                }
            }
        );
    };

    handleChange = (e,value) => {
        switch(value){
            case 1:
                this.setState({
                    name:e.target.value
                });
                break;

            case 2:
                this.setState({
                    organization:e.target.value
                });
                break;

            case 3:
                this.setState({
                    phone:e.target.value
                });
                break;

            case 4:
                this.setState({
                    email:e.target.value
                });
                console.log(value);
                break;
        }

    };

    render() {
        return(
            <Layout>
                <SEO title="Persona Three" />
                <Navbar />
                <div className="sidenav" style={{backgroundImage: `url(${Background})`}}>
                    <div className="login-main-text" align="center">
                        <Link className="navbar-brand" to="#">
                            <img src={logo} width="400px" height="70px" alt="logo image"/>
                        </Link>
                    </div>
                </div>
                <div className="main">
                    <div className="col-md-10 offset-md-1 col-sm-12">
                        <div className="login-form">
                            <div className="form-text mb-4">
                                <p className="form-text-body">Unlock your cash.</p>
                                <p className="form-text-head">Do you have cash locked in unpaid invoices, with new orders pending ?</p>
                                <p className="form-text-last">We will advance your pending payments so that you can fulfill those new orders.</p>
                            </div>
                            <Form  id="myForm" onSubmit={this.handleClick.bind(this)}>
                                <Form.Group controlId="formBasicName">
                                    <Form.Control type="text" placeholder="Full Name" required
                                                  value={this.state.name}
                                                  onChange={(event) => this.handleChange(event,1)} />
                                </Form.Group>

                                <Form.Group controlId="formBasicCompany">
                                    <Form.Control type="text" placeholder="Company Name" required
                                                  value={this.state.organization}
                                                  onChange={(event) => this.handleChange(event,2)} />
                                </Form.Group>

                                <Form.Group controlId="formBasicPhone">
                                    <Form.Control type="number" placeholder="Phone Number" required                                                   value={this.state.phone}
                                                  onChange={(event) => this.handleChange(event,3)} />
                                </Form.Group>

                                <Form.Group controlId="formBasicEmail">
                                    <Form.Control type="email" placeholder="Email Address" required
                                                  value={this.state.email}
                                                  onChange={(event) => this.handleChange(event,4)} />
                                </Form.Group>

                                <Button type="submit" variant="btn btn-primary mt-lg-4 float-right" id="form-btn">
                                    <i className="fa fa-handshake"/>
                                    {" "}GET STARTED
                                </Button>
                            </Form>
                        </div>
                    </div>
                </div>
            </Layout>
        );
    }
}

export default ThirdPage
