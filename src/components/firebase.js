import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyDABdRJ9aQJBO9zlRyFdQcR8nDRP1nOPbQ",
    authDomain: "nvoicia-social-media-campaign.firebaseapp.com",
    databaseURL: "https://nvoicia-social-media-campaign.firebaseio.com",
    projectId: "nvoicia-social-media-campaign",
    storageBucket: "",
    messagingSenderId: "386674404671",
    appId: "1:386674404671:web:f8ab236273f8684e"
};

firebase.initializeApp(firebaseConfig);

export default firebase;