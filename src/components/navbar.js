import React from 'react';

class Navbar extends React.Component{

	render(){
		return (
			<nav className="navbar navbar-light bg-light" id="navbar">
				<a ></a>
			    <a className="navbar-phone mr-4" href="tel:+233 50 055 0559">
			        <i className="fa fa-phone-alt"></i>
			        {" "}Call Us +233 50 055 0559
			    </a>
			</nav>
		);
	}
}

export default Navbar